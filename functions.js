function statisticsWithoutRange(array) {
    resultArray = []
    for (var i = 0; i < array.length; i++) {
        let websiteRecord = array[i];
        if (resultArray.length === 0) {
            websiteRecord = {
                "websiteId": websiteRecord.websiteId,
                "chats": websiteRecord.chats,
                "missedChats": websiteRecord.missedChats
            }
            resultArray.push(websiteRecord)
        } else {
            var arrFound = Object.keys(resultArray).filter(function(key) {
                return resultArray[key].websiteId == websiteRecord.websiteId;
            })
            if (arrFound.length > 0) {
                const arrayID = arrFound[0]
                resultArray[arrayID].chats = resultArray[arrayID].chats + websiteRecord.chats
                resultArray[arrayID].missedChats = resultArray[arrayID].missedChats + websiteRecord.missedChats
            } else {
                websiteRecord = {
                    "websiteId": websiteRecord.websiteId,
                    "chats": websiteRecord.chats,
                    "missedChats": websiteRecord.missedChats
                }
                resultArray.push(websiteRecord)
            }
        }
    }
    return resultArray

}

function statisticsWithRange(array, start, end) {
    resultArray = []
    for (var i = 0; i < array.length; i++) {
        let websiteRecord = array[i];
        const websiteRecordDate = new Date(websiteRecord.date)

        if (websiteRecordDate >= start && websiteRecordDate <= end) {
            if (resultArray.length === 0) {
                websiteRecord = {
                    "websiteId": websiteRecord.websiteId,
                    "chats": websiteRecord.chats,
                    "missedChats": websiteRecord.missedChats
                }
                resultArray.push(websiteRecord)
            } else {
                var arrFound = Object.keys(resultArray).filter(function(key) {
                    return resultArray[key].websiteId == websiteRecord.websiteId;
                })
                if (arrFound.length > 0) {
                    const arrayID = arrFound[0]
                    resultArray[arrayID].chats = resultArray[arrayID].chats + websiteRecord.chats
                    resultArray[arrayID].missedChats = resultArray[arrayID].missedChats + websiteRecord.missedChats
                } else {
                    websiteRecord = {
                        "websiteId": websiteRecord.websiteId,
                        "chats": websiteRecord.chats,
                        "missedChats": websiteRecord.missedChats
                    }
                    resultArray.push(websiteRecord)
                }
            }
        }
    }
    return resultArray
}

module.exports.statisticsWithRange = statisticsWithRange
module.exports.statisticsWithoutRange = statisticsWithoutRange