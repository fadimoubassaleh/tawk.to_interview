// 
// auth: Fadi Mabsaleh <fadimoubassaleh@gmail.com>
// 
// description: the main file of the server
// use:     copy .example.env -> change name to .env -> change all the variables
//          
// 
// 

const express = require('express');
const app = express();
const dotenv = require('dotenv'); // env
const fs = require("fs"); // file system
const joi = require('@hapi/joi') // VALIDATION 
const readline = require('readline');
const functions = require('./functions')

dotenv.config(); // run env variables

// Middleware
app.use(express.json());
//

// read JSON file + parse it
const content = fs.readFileSync('data/webstats.json');
const JSONContent = JSON.parse(content);
// 

// read TXT file
let TXTContent = []
const stream = fs.createReadStream('./data/webstats.txt');
// stream.on('error', reject);
const reader = readline.createInterface({
    input: stream
});

reader.on('line', line => {
    let item = line.split(',')
    item = {
        "websiteId": item[0],
        "date": item[1],
        "chats": JSON.parse(item[2]),
        "missedChats": JSON.parse(item[3])
    }
    TXTContent.push(item);
})

// 

// validation schema
const schema = joi.object({
        start: joi.date(),
        end: joi.date()
    })
    // 

// API to use json file and get the analytics that want
app.post('/json', function(req, res) {
    let resultArray = [] // array to save the result in

    if (req.body.start && req.body.end) { // check if the request with date range
        // validate body variables
        const { error } = schema.validate(req.body);
        if (error) {
            return res.status(400).json({
                success: false,
                message: [
                    error.details[0].message
                ]
            })
        }
        const start = new Date(req.body.start) // the start date form the request
        const end = new Date(req.body.end) // the end date form the request

        resultArray = functions.statisticsWithRange(JSONContent, start, end)
    } else {
        resultArray = functions.statisticsWithoutRange(JSONContent)
    }

    // send response
    try {
        res.status(200).send(resultArray)
    } catch (err) {
        res.status(400).json({
            success: false,
            message: [
                error.details[0].message
            ]
        })
    }

});
// 

// API to use json file and get the analytics that want
app.post('/txt', function(req, res) {
        let resultArray = [] // array to save the result in

        if (req.body.start && req.body.end) { // check if the request with date range
            // validate body variables
            const { error } = schema.validate(req.body);
            if (error) {
                return res.status(400).json({
                    success: false,
                    message: [
                        error.details[0].message
                    ]
                })
            }
            const start = new Date(req.body.start) // the start date form the request
            const end = new Date(req.body.end) // the end date form the request

            resultArray = functions.statisticsWithRange(TXTContent, start, end)
        } else {
            resultArray = functions.statisticsWithoutRange(TXTContent)
        }

        // send response
        try {
            res.status(200).send(resultArray)
        } catch (err) {
            res.status(400).json({
                success: false,
                message: [
                    error.details[0].message
                ]
            })
        }
    })
    // 

// run server (node - express)
// server variables
const port = process.env.SERVER_PORT
    // 
app.listen(port, () => console.log('Express run on port ' + port))
    //